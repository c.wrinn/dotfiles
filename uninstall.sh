#!/bin/bash
cd "$(dirname "$0")"
SCRIPTPATH="$(pwd -P )"

for file in .*; do
  if [[ ${file} == '.' || ${file} == '..' ]]; then continue; fi

  homefile=${HOME}'/'${file}
  
  if [[ -h ${homefile} ]]; then
    rm -f ${homefile}

    if [[ -e "${homefile}.orig" ]]; then
      mv ${homefile}.orig ${homefile}
    elif [[ -e "/etc/skel/${file}" ]]; then
      cp /etc/skel/${file} ${homefile}
    fi
  fi
done
