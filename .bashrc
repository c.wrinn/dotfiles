#-------------------------------------------------------------
# Source global definitions (if any)
#-------------------------------------------------------------
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

#-------------------
# Personnal Aliases
#-------------------
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi


#-------------------
# Bash Functions
#-------------------
if [ -f ~/.bash_functions ]; then
    . ~/.bash_functions
fi

#-------------------
# Bash Completions
#-------------------
if [ -f ~/.bash_completions ]; then
    . ~/.bash_completions
fi

#-------------------
# Start Screen
#-------------------
if [ "$PS1" != "" -a "${STARTED_SCREEN:-x}" = x -a "${SSH_TTY:-x}" != x ]
then
  STARTED_SCREEN=1 ; export STARTED_SCREEN
  [ -d $HOME/lib/screen-logs ] || mkdir -p $HOME/lib/screen-logs
  sleep 1
  screen && exit 0
  # normally, execution of this rc script ends here...
  echo "Screen failed! continuing with normal bash startup"
fi
